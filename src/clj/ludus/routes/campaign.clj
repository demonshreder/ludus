(ns ludus.routes.campaign
  (:require [ludus.layout :as layout]
            [ludus.db.core :as db]
            [compojure.core :refer [defroutes GET POST]]
            [ring.util.response :refer [response]]
            [clojure.java.io :as io]))
  
  ;(-> (ring.util.response/redirect "/")
   ;   (assoc :headers {"Content-Type" "text/html"})))

(defn home-page [supported]
  (let [sup-count (db/count-support!)]
    (let [peeps (:count sup-count) orgs (:count_2 sup-count)]
     (-> (response (layout/home peeps orgs (* (quot (+ peeps 99) 100) 100) supported))
        (assoc :headers {"Content-Type" "text/html"})))))


(defn home-page-ta [supported]
  (let [sup-count (db/count-support!)]
    (let [peeps (:count sup-count) orgs (:count_2 sup-count)]
     (-> (response (layout/home-ta peeps orgs (* (quot (+ peeps 99) 100) 100) supported))
        (assoc :headers {"Content-Type" "text/html"})))))


(defn support-page []
  (-> (response (layout/support))
      (assoc :headers {"Content-Type" "text/html"})))

(defn add-support [data]
  (db/show-support! (if-let [form (= (:subscribe data) "on")] (assoc data :subscribe true) (assoc data :subscribe false)))
  (home-page true))


(defn show-signatories []
;  (let [a (db/show-signatories!)]
   (-> (response (layout/signatories-page "sexy"))
      (assoc :headers {"Content-Type" "text/html"})))
  
;(defn home-page []
 ; (layout/render
  ;  "home.html" {:docs (-> "docs/docs.md" io/resource slurp)}))

(defn about-page []
  (layout/render "about.html"))

(defroutes home-routes
  (GET "/" [] (home-page false))
  (GET "/ta" [] (home-page-ta false))
  (GET "/support" [] (support-page))
  (POST "/support" data (add-support (:params data)))
  (GET "/signatories" [] (show-signatories)))
  

