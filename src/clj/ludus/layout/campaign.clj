(ns ludus.layout.campaign
  (:require [selmer.parser :as parser]
            [selmer.filters :as filters]
            [markdown.core :refer [md-to-html-string]]
            [ring.util.http-response :refer [content-type ok]]
            [ring.util.anti-forgery :refer [anti-forgery-field]]
            [ring.middleware.anti-forgery :refer [*anti-forgery-token*]]
            [ludus.db.core :as db])
  (:use
   [hiccup.core :only (html)]
   [hiccup.page :only (html5 include-css include-js)]
   [ring.util.anti-forgery]))

(defn base [title & content]
  (html5 {:lang "en"}
         [:head
          [:meta {:charset "utf-8"}]
          [:meta {:name "viewport" :content "width=device-width, initial-scale=1"}]
          [:link {:rel "icon" :type "image/png" :href "/img/FSHM_logo.png"}]
          [:title title]
          (include-css "/css/bulma.min.css")]
         [:body.has-background-light ;{:style "background:rgb(216, 39, 56)"}
          ;;[:a {:href "/"} [:figure {:class "image is-64x64"}[:img {:src "/img/FSHM_logo.png"}]]]]]
          ;; (include-css "https://cdn.jsdelivr.net/npm/fork-awesome@1.1.5/css/fork-awesome.min.css")]
          ;;  (include-js "js/script.js")
          ;; [:body {:class "has-navbar-fixed-top"}
          ;;  [:nav {:class "navbar is-fixed-top"}
          ;;   [:div {:class "navbar-brand"}
          ;;    [:a {:class "navbar-item" :href "/"}
          ;;     [:img {:src "/img/FSHM_logo.png"}]]]
          ;;   [:a {:role "button", :class "navbar-burger burger", :aria-label "menu", :aria-expanded "false", :data-target "navbarBasicExample"}
          ;;    [:span {:aria-hidden "true"}]
          ;;    [:span {:aria-hidden "true"}]
          ;;    [:span {:aria-hidden "true"}]]]
          ;; [:div {:class "navbar-end is-right"}
          ;;  [:a {:class "navbar-item" :href "/login"}
          ;;   "Login"]
          ;;  [:a {:class "navbar-item" :href "/register"}
          ;;  "Register"]]]
          content
          [:br][:br]
          [:h1 {:class "title is-size-3 has-text-centered"} "Public interest initiative by"]
          [:div.columns.is-centered
           
           [:div.column.is-one-quarter
            [:div.card
             [:div.card-image
              [:figure.image.is-1by1
               [:img
                {:alt "FSHM logo",
                 :src "/img/FSHM_logo.png"}]]]
             [:div.card-content
              
              [:div.media-content
               [:p.title.is-4 "FSHM"]
               [:p.subtitle.is-6 "Free Software Hardware Movement Puducherry"]
               [:div.content.has-text-justified  "\n   FSHM conducts free weekly sessions and works to bring awareness about digital security, privacy and economic sustenance through Free and Open Technologies. It conducts frequent public exhibitions, mapathons and hackathons. "]
               [:footer.card-footer
                [:a.card-footer-item {:href "https://www.fshm.in"} "Home\n      "]
                [:a.card-footer-item {:href "https://blog.fshm.in"} "Blog"
                 " "]
                [:a.card-footer-item {:href "https://riot.im/app/#/room/#fshm:matrix.org"} "Chat"
                 " "]
                [:a.card-footer-item {:href "https://www.freelists.org/list/puduvailug"}
                 "Mail"]

                [:br]]]]]]
           [:div.column.is-one-quarter
            [:div.card
             [:div.card-image
              [:figure.image.is-4by5
               [:img
                {:alt "FSFTN logo",
                 :src "/img/FSFTN_logo.png"}]]]
             [:div.card-content
              
              [:div.media-content
               [:p.title.is-4 "FSFTN"]
               [:p.subtitle.is-6 "Free Software Foundation Tamil Nadu"]
               [:div.content.has-text-justified  "\n   FSFTN has presence in 7 cities in Tamil Nadu, where it conducts free monthly sessions on Free & Open Technologies. FSFTN has given its expert opinion on numerous technosocial issues including IT Act, Net Neutrality, TN free laptop scheme, FB data breach, EVMs."]
               [:footer.card-footer
                [:a.card-footer-item {:href "https://fsftn.org"} "Home"  "\n      "]
                [:a.card-footer-item {:href "https://fsftn.org/blog"} "Blog"
                 " "]
                [:a.card-footer-item {:href "https://riot.im/app/#/room/#fsftn:matrix.org"} "Chat"
                 " "]
                [:a.card-footer-item {:href "https://discuss.fsftn.org"}
                 "Forum"
                 " "]
                [:br]]]]]]]]))


(defn home [peeps orgs total supported & content]
  (base "FSHM"
        ;; (include-css "https://cdn.jsdelivr.net/npm/fork-awesome@1.1.5/css/fork-awesome.min.css")]
        [:section {:class "section"}
         [:div {:class "has-text-centered"}
          [:div {:class "container"}         
           [:a.is-size-8 {:href "/ta" } "தமிழில்"][:br]
           [:p {:class "title is-size-1 has-text-success"} "Your call for a free, private & secure Internet has reached the IT Ministry"]
           ;;         (if supported (html [:p {:class "title is-size-1 has-text-success"} "Thank you for supporting a private & secure Internet for all of us"]) (html [:p {:class "title is-size-1"} "Sign the open letter to protect your right to a private & secure Internet"]))
           
           [:p {:class "is-size-3"}  "IT Ministry confirms receipt of our open letter in the screenshot below"]
           [:br
            [:figure.image [:img {:src "/img/meity_confirmation.png" :style "width:100%;height:auto;margin:auto;"}]][:br]]
           
           [:a {:class "button is-large is-info" :href "/signatories"}  "See who signed"]
           [:br][:br]
           [:a {:class "button is-large is-info" :href "/pdf/meity_letter.pdf"}  "Read the letter"]
           [:br][:br]
           [:h2 {:class "title"} peeps  "  signatures received"]
           ;; [:progress.progress.is-warning {:value peeps :max peeps}]            [:br]
           [:a.is-size-8 {:href "https://files.fshm.in/s/qfTzWxqcc4e5nmd" } "Listen to our Tamil podcast about these changes"][:br]
           
           [:a.is-size-8 {:href "https://sflc.in/faq-draft-amendment-intermediary-guidelines-rules-india" } "Read the detailed FAQ by sflc.in on these changes"][:br]
           [:a.is-size-8 {:href "http://meity.gov.in/content/comments-suggestions-invited-draft-%E2%80%9C-information-technology-intermediary-guidelines" } "IT Ministry's official announcement"][:br]

           [:a.is-size-8 {:href "https://saveourprivacy.in/blog/explainer-with-new-rules-to-snoop-and-censor-on-online-content-india-is-moving-towards-the-chinese-model-of-a-closed-internet" } "Read about the social impacts of these changes at saveourprivacy.in"]]]]
        
        [:section {:class "section"}
                                        ;[:br]
         [:h1 {:class "title is-size-1 has-text-centered"}
          "Problems in the new changes"]
                                        ;[:h3 {:class "title"} "#RightToMeme"]
         [:h2 {:class "subtitle is-size-3 has-text-centered"}
          "The new changes would make Internet companies the guardians the ultimate authority on what we can see / share / consume on the Internet. This is wrong."]
                                        ; [:a {:class "button is-large is-primary" :href "/support"}  "Sign the letter"]
         [:div {:class "columns is-centered"}
          [:div {:class "column is-three-quarters"}
           [:br]
           
           [:h1 {:class "title has-text-centered"}
            "You may no longer be able to create / share memes"]
           [:figure.image [:img {:src "/img/meme-comic-it-jail.png"}]]
           [:h2 {:class "subtitle has-text-danger has-text-justified"} "Memes are an itegral part of today's Internet experience. While they are funny and easy to connect with, they usually contain scenes from movies or faces of people. They also tend to make fun of social / political events. The changes require automated blocking of such things, which could mean that memes would no longer be allowed."]
           [:br]
           
           [:h1 {:class "title has-text-centered"}
            "Government can prevent people from protesting for their livelihood"]
           [:figure.image [:img {:src "/img/livelihood-protest.gif" :style "width:50%;height:auto;margin:auto;"}]][:br]
           [:h2 {:class "subtitle has-text-danger has-text-justified"}
            "The Jallikattu protests in Tamilnadu were possible only because of personal messaging & social media platforms. This was well understood by the TN government which shutdown Internet and electricity in the entire Thoothukudi district during the Sterlite protests. A lot of unconstitutional & immoral things happen when government restricts the flow of information. The proposed changes require service providers to interrupt their service to mass or targeted individuals whenever required by the government. Internet is a public utility just like water or electricity, this is undue and unjust power in the hands of government and corporations."]
           [:br]
           [:h1 {:class "title has-text-centered"}
            "Government can access all your private data"]
           [:h2 {:class "subtitle has-text-danger has-text-justified"}
            "If government agencies are empowered to request data as they seem fit, this opens up avenue for misuse. We don't have a strong data protection bill yet thus there are no safeguards on what should or should not be done. Supreme court has struck down Section 66A of the IT Act but government agencies have continued to use that law. Supreme Court has forbidden the use of Aadhar in non-subsidy matters but government regulations to telecom companies and banks require Aadhar."]
           
           [:br]
           [:h1 {:class "title has-text-centered"}
            "Facebook & Google can now store your private data forever"]
           [:h2 {:class "subtitle has-text-danger has-text-justified"}
            "The proposed changes have extended the period for data retention by the service providers / Intermediaries from 90 days to 180 days or 'as required'. This empowers companies to store private data without permission of the user. Facebook and Google make their money by selling private data to advertisers. The proposed changes can make their immoral actions legal."]
           [:br]
           [:h1 {:class "title has-text-centered"}
            "Your access to Internet based services can be blocked automatically"]
           [:h2 {:class "subtitle has-text-danger has-text-justified"}  "Internet has now become an integral part of our life. The proposed changes put the companies in charge what can be spoken or shared on the Internet platforms. The proposed changes require the service providers to terminate user access to their services - Internet, email, chat, social media, file hosting, web search -  based on an arbritrary criteria."]]]]))

                                        ;[:br]
;; Home


(defn home-ta [peeps orgs total supported & content]
  (base "FSHM"
        ;; (include-css "https://cdn.jsdelivr.net/npm/fork-awesome@1.1.5/css/fork-awesome.min.css")]
        [:section {:class "section"}
         [:div {:class "has-text-centered"}
          [:div {:class "container"}         

           [:p {:class "title is-size-1 has-text-success"} "கட்டற்ற, பாதுகாப்பான இணையத்தை மீட்க தேவையான நம் கருத்தை தகவல் தொழில்நுட்ப அமைச்சகத்தில் சமர்க்கிப்பட்டது "
            ;;         (if supported (html [:p {:class "title is-size-1 has-text-success"} "Thank you for supporting a private & secure Internet for all of us"]) (html [:p {:class "title is-size-1"} "Sign the open letter to protect your right to a private & secure Internet"]))
            
            [:p {:class "is-size-3"}  "தகவல் தொழில்நுட்ப அமைச்சகம் நம் கருத்தை சமர்க்கிப்பட்ட சான்று"]
            [:br
             [:figure.image [:img {:src "/img/meity_confirmation.png" :style "width:100%;height:auto;margin:auto;"}]][:br]]
            
            [:a {:class "button is-large is-info" :href "/signatories"}  "கையொப்ப பட்டியல்"]
            [:br][:br]
            [:a {:class "button is-large is-info" :href "/pdf/meity_letter.pdf"}  "கடிதத்தை படிக்க"]
            [:br]]
           ;;[:br]]

           ;;           (if supported (html [:p {:class "title is-size-1 has-text-success"} "Thank you for supporting a private & secure Internet for all of us"]) (html [:p {:class "title is-size-1"} "உங்களின் உரிமையான பாதுகாப்பான இணையத்துக்காக இக்கடிதத்தில் கையொப்பமிடிவும்"]))
           
           ;;[:p {:class "is-size-3"}  "இந்திய தகவல் தொழில்நுட்ப அமைச்சகம் தகவல் தொழில்நுட்ப சட்டத்தில் கொண்டுவந்துள்ள சட்ட முன்வரைவில் பொது மக்களின் கருத்தை வருகிற சனவரி 31 வரை கேட்டுள்ளது."]
           ;;[:br]
           ;;[:a {:class "button is-large is-info" :href "/support"}  "கடிதத்தில் கையொப்பமிடவும்"]
           [:br][:br]
           [:h2 {:class "title"} peeps  "  கையொப்பங்கள் சமர்க்கிப்பட்டன"]
           [:progress.progress.is-warning {:value peeps :max total}]            [:br]
           [:a.is-size-8 {:href "https://files.fshm.in/s/qfTzWxqcc4e5nmd" } "சட்டமுன்வரைவை பற்றி அறிய இக்கருத்தொலியை (podcast) கேட்கவும்"][:br]
           
           [:a.is-size-8 {:href "https://sflc.in/faq-draft-amendment-intermediary-guidelines-rules-india" } "முன்வரைவால் ஏற்படும் தீமையான மாற்றங்களைப்பற்றி SFLC இன் கட்டூரையை படிக்கவும்"][:br]
           [:a.is-size-8 {:href "http://meity.gov.in/content/comments-suggestions-invited-draft-%E2%80%9C-information-technology-intermediary-guidelines" } "தகவல் தொழில்நுட்ப அமைச்சகத்தின் பொது அறிவிப்பு"][:br]

           [:a.is-size-8 {:href "https://saveourprivacy.in/blog/explainer-with-new-rules-to-snoop-and-censor-on-online-content-india-is-moving-towards-the-chinese-model-of-a-closed-internet" } "இம்மாற்றத்தால் ஏற்படக்கூடிய சமூக தாக்கங்கள் மற்றும் விளைவுகளை பற்றி படிக்கவும்"]]]]
        
        [:section {:class "section"}
                                        ;[:br]
         [:h1 {:class "title is-size-1 has-text-centered"}
          "முன்வரைந்த புதிய மாற்றங்களில் நிலவும் பிரச்சனைகள்"]
                                        ;[:h3 {:class "title"} "#RightToMeme"]
         [:h2 {:class "subtitle is-size-3 has-text-centered"}
          "
இணையத்தை கார்ப்பரேட் கம்பனிகளின் கட்டுப்பாட்டில் வைத்திருக்கவும், அவர்களையே அதிகாரப்பூர்வ கொடுங்கண்காணிப்பாளராக மாற்றவும் பலமளிக்கும். இது சமூக அநீதியாகும்.
"]
                                        ; [:a {:class "button is-large is-primary" :href "/support"}  "Sign the letter"]
         [:div {:class "columns is-centered"}
          [:div {:class "column is-three-quarters"}
           [:br]
           
           [:h1 {:class "title has-text-centered"}
            "நீங்கள் இனி மீம் (meme)களை, வரையவோ, பகிரவோ முடியாது
"]
           [:figure.image [:img {:src "/img/meme-comic-it-jail.png"}]]
           [:h2 {:class "subtitle has-text-danger has-text-justified"} "மீம்கள் இன்று இணையத்தோடு ஒன்றிணைந்த பண்பாகும். அவை  நகைச்சுவையோடு நம் எண்ணங்களை தொடர்பு படுத்தினாலும், பெரும்பாலும் திரைபடங்களிலிருந்தோ, மற்ற நபர்களின் முகங்களை வைத்தோ இயற்றப்படுகின்றன. மக்கள் மீம்களைவைத்து சமூக மற்றும் அரசியல் நிகழ்வுகளை சித்தரிக்கமுடிந்தது. ஆனால் இம்முன்வரைவில் கூறும் மாற்றத்தால், மீம்களை இணையத்திலிருந்து நீக்கவும், தடைசெய்யவும் முடியும். 
"]
           [:br]
           
           [:h1 {:class "title has-text-centered"}
            "ஆட்சியில் உள்ள அரசு மக்களை தங்களின் உரிமைக்காக போராடுபவதை தடுக்கவும்/தவிர்க்கவும் முடியும்"]
           [:figure.image [:img {:src "/img/livelihood-protest.gif" :style "width:50%;height:auto;margin:auto;"}]][:br]
           [:h2 {:class "subtitle has-text-danger has-text-justified"}
            "தமிழ்நாட்டில் நிகழ்ந்த சல்லிக்கட்டு போராட்டம், மக்கள் பயன் படுத்திய சமூக ஊடகமே காரணாமாகும். இதனை புரிந்துணர்ந்ததால் தால் தமிழ்நாடு அரசு தூத்துக்குடியின் ஸ்டெர்லைட் (sterlite) போராட்டங்களின் போது இணையத்தையும், மின் வசதியையும்  துண்டித்து. அரசு தகவலை கொடுங்கட்டுப்பாட்டில் வைத்திருக்கும் வரை பல்வேறு அரசிஅல் அமைப்புக்கு எதிரான நெறியற்ற நடந்தேற மிகையான வாய்ப்புளது.  
"]
           [:br]
           [:h1 {:class "title has-text-centered"}
            "அரசாங்கம் உங்களின் அகவுரிமை (privacy) மற்றும் அந்தரங்க (private) தகவல்களை சரண்ட - கொடுங்கண்காணிப்பில் ஈடுபடும். "]
           [:h2 {:class "subtitle has-text-danger has-text-justified"}
            "அரசாங்கம் ஞாயமற்ற காரணங்களுக்காக அதன் வசதிக்கேற்ப இச்சட்ட மாற்றத்தை தீய நோக்கங்களுக்காக பயன்படுத்த முடியும். இதுவரை இந்தியாவில் முறையான தகவல் பாதுகாப்பு சட்டம் நிறைவேற்ற படாத நிலையில், இத்தகைய சட்ட முனைவரைவு நிறைவேறினால் அரசு முறையான சட்ட முறையை கையாண்டிருக்கிறதே என நம்மால் கூற இயலாது. நாட்டின் உச்ச நீதிமன்றம், தகவல் தொழில்நுட்ப சட்டத்தின் 66A பிரிவை நீக்கய பின்பும், அரசு துறைகள் இன்றும் அவைகளை தேவையற்று கட்டாயமாக்குகின்றன. முறையே ஆதாரை மானியமில்லா சேவைகளுக்கு பயன்படுத்த கூடாது என ஆண்யிட்டும், இன்றளவும், தொலை தொடர்பு நிறுவனங்களும், வங்கிகளும் மக்களை கட்டாயபடுத்தி துன்புறுத்துகின்றன. "]
           
           [:br]
           [:h1 {:class "title has-text-centered"}
            "ஃபேஸ்புக்கும் (facebook) மற்றும் கூகுள் (google) கார்ப்பரேட் நிறுவனங்கள் இம்மாற்றத்தால் சட்டபூர்வமாக உங்களின் அக மற்றும் அந்தரங்க தகவல்களை காலவரையின்றி தக்கவைத்துக்கொள்ள முடியும்.
"]
           [:h2 {:class "subtitle has-text-danger has-text-justified"}
            "முன்வரைந்துள்ள மாற்றங்கள், இணைய வியாபார நிறுவனங்களுக்கு, தகவல் தக்கவைக்கும் கால அளவை 90 நாட்களிலிருந்து 180 நாட்களாக அல்லது தேவைக்கேற்ப  நீட்டிக்கொள்ளலாம் என உரிமையளிக்கும். இதனால் இன்நிறுவனங்கள் பயனாளர் (நீங்கள் தான்) அனுமதியின்றி தகவலை தக்க வைத்துக்கொள்ள முடியும். ஃபேஸ்புக்கும் (facebook) மற்றும் கூகுள் (google) பயனாளரின் அனுமதியின்றி தகவலை விளம்பரதார்களுக்கு விற்று இலாபத்தை ஈட்டுகின்றனர். இச்சட்ட முன்வரைவு மாற்றம் செய்படுத்தப்படின், மேற்கூறிய நீதியற்ற செயல்களுக்கு சட்டபூர்வமான அங்கீகாரம் அளிக்கப்படும்."]
           [:br]
           [:h1 {:class "title has-text-centered"}
            "நீங்கள் பயன்படுத்தும் ஏனைய இணைய சேவைகளை, தொழில்நுட்ப உதவியோடு நீக்கவம்/தடைசெய்யவும் முடியும்."]
           [:h2 {:class "subtitle has-text-danger has-text-justified"}  "இணையம் நம் வாழ்வின் இன்றியமையா பகுதியாகிவிட்டது. முன்வரையப்பட்டுள்ள சட்ட மாற்றங்கள், கார்ப்பரேட் நிறுவனங்களுக்கு யார் என்ன பேச/பகிர வேண்டும், எதனை பேச/பகிர வேண்டும், என்பதை நிர்ணயிக்கும் ஞாயமற்ற பலத்தை அளிக்கும். மற்றும் அனைத்து இணைய சேவை நிறுவனங்களும் பயனாளரை  எவ்வித இணைய சேவையை பயன்படுத்திலிருந்து முடக்க சட்டபூர்வமாக முடக்க நேரிடும். இத்தகைய செயல் நெறியற்றதாகும்."]]]]))

                                        ;[:br]
;; Home-ta


(defn support [& content]
  (base "FSHM"
        ;; (include-css "https://cdn.jsdelivr.net/npm/fork-awesome@1.1.5/css/fork-awesome.min.css")]
        ;;  (include-js "js/script.js")
        ;;[:section {:class "hero center"}
        ;;[:div {:class "hero-body"}
        [:div {:class "container"}
         [:br]
                                        ;[:h1 {:class "title has-text-centered is-size-1"}"#RightToMeme"]
         [:h1 {:class "title has-text-centered is-size-1"}
          "Your support makes the Internet safe and secure for all of us"
          [:h2 {:class "subtitle has-text-centered is-size-3"}
           "Sign the open letter for a private and secure Internet "
           [:br][:br]]]
         [:div {:class "columns"} 
          [:div {:class "column is-one-third is-offset-one-third"}
           [:form {:method "post" :action "/support"}
            (anti-forgery-field)
            
            [:div {:class "field"}
             [:label {:class "label"} "Name"]
             [:div {:class "control"}
              [:input {:class "input is-large is-info", :name "name" :type "text", :placeholder "Ramya", :required "true"}]]]
            [:div {:class "field"}
             [:label {:class "label"} "Email"]
             [:div {:class "control"}
              [:input {:class "input is-large is-info", :name "email" :type "email", :placeholder "ramya76@fshm.in", :required "true"}]]]
                                        ;[:br]
            [:div {:class "field"}
             [:label {:class "label"} "Organization"]
             [:div {:class "control"}
              [:input {:class "input is-large is-info",:name "org" :type "text", :placeholder "FSHM"}]]]
            [:div {:class "field"}
             [:div {:class "control"}
              [:input {:class "checkbox is-large is-info",:name "subscribe" :checked "true" :type "checkbox"}]
              [:label " I'd like to receive email updates from FSHM or FSFTN"]]]
            ;;[:br]
            [:div {:class "control"} [:input {:type "submit" :value "Sign" :class "button is-large is-info"}]]]]]]
        [:section {:class "section"}
         [:div.container
                                        ;[:br]
          [:h1 {:class "title is-size-1 has-text-centered"}
           "Letter Summary"]
                                        ;[:h3 {:class "title"} "#RightToMeme"]
          [:h2 {:class "subtitle is-size-3 has-text-centered"}
           "The IT Ministry should enact a strong data protection bill. It shouldn't enforce technocratic solutions which endanger Indians and offload the repsonsibilities of the government to Internet companies and services providers. Such a scenario only legalizes mass surveillance, data theft and automates censorship."]
                                        ; [:a {:class "button is-large is-primary" :href "/support"}  "Sign the letter"]
          [:div {:class "columns is-centered"}
           [:div {:class "column is-three-quarters has-text-centered"}
            [:br]
            [:h1 {:class "title has-text-centered"}
             "Only court orders should be allowed to request private information of Indian citizens"]
            [:h2 {:class "subtitle has-text-danger has-text-justified"}
             "The amendments empower any 'appropriate government' with 'lawful order' but don't define any process for determing if an order is indeed lawful. Numerous websites have been ordered to be blocked by ISPs and search engines by the government even though this has been declared illegal many times by the courts. The current situation would only worse if the proposed changes are to be passed. "]
            [:a.is-size-8 {:href "https://sflc.in/supreme-court-issues-notice-centre-continued-use-section-66a-it-act-2000" } "Read: Supreme Court Issues Notice to the Centre for Continued use of Section 66A of the IT Act, 2000"][:br]
            [:br]
            [:a.is-size-8 {:href "https://sflc.in/internet-archive-among-2650-websites-blocked-copyright-infringement" } "Read: 
The Internet Archive is among 2650 websites blocked for copyright infringement
"]
            [:br]
            [:br]
            [:h1 {:class "title has-text-centered"}
             "Automatic measures should not be implemented"]
            [:h2 {:class "subtitle has-text-danger has-text-justified"}  "Today's artificial intelligence guided automatic systems are biased and dsyfunctional. There are numerous reports of Original Content Creators (OCC) being punished because of the automated system's mistake. Measures like these demand mass surveillance by ethical Intermediaries. Such requirements are detrimental to the welfare and growth of Indian society, especially when such measures can't be implemented by new entrants to Indian Internet sphere."]
            
            [:a.is-size-8 {:href "https://www.eff.org/deeplinks/2019/01/dont-put-robots-charge-internet" } "Read: 
Don’t Put Robots in Charge of the Internet

"][:br
            [:br]
            [:a.is-size-8 {:href "https://boingboing.net/2019/01/09/lionsgate-accused-of-abusing-y.html" } "Read:
Lionsgate accused of abusing YouTube's contentID system to remove criticism of its movies"]
            [:br]   
            [:br]
            [:a.is-size-8 {:href "https://www.eu-startups.com/2019/01/article-13-how-startups-would-be-affected-by-such-a-copyright-reform/" } "Read:
 How startups would be affected by such a Copyright Reform
"][:br
            [:br]
            [:br]
            [:h1 {:class "title has-text-centered"}
             "Identifying source of information should not endanger privacy"]
            [:h2 {:class "subtitle has-text-danger has-text-justified"}
             "The proposed changes put the responsibility of identifying the source / originator of information to the service providers. This gives legal basis for the service providers to break End-to-End encryption and continue to steal private information from users. Encryption should not be broken / undermined in any way."]
            [:br]
            [:h1 {:class "title has-text-centered"}
             "Data retention period should not be increased"]
            [:h2 {:class "subtitle has-text-danger has-text-justified"}  "The need to retain data by the Intermediaries when required by law creates an ambiguity on applicability of cases & time limit on storage of private information. This can be exploited to store vast amounts of private information. "]
            [:br]
            [:h1 {:class "title has-text-centered"}
             "Intermediaries shouldn't have the right to terminate services when 'deemed necessary'"]
            [:h2 {:class "subtitle has-text-danger has-text-justified"}
             "The proposed changes require the Intermediaries to block user access to services including communication, social media, Internet etc. Internet has been recognized as a public utility around the world, just like water or electricity. Such an important resource shouldn't be made to be disconnected based on criteria deemed ambiguous by the Supreme Court."]]]]]]]))           
;; support-page

(defn signatories-page [signatories & content]
  (base "FSHM" [:section.section
                
                [:div.columns.is-centered
                 [:div.column.is-half
                  [:h1.title.is-size-1.has-text-centered "Signatories"]
                  [:table.table
                   [:thead
                    [:tr
                     [:th "Name"]
                                        ;                     [:th "Email"]
                     [:th "Organization"]]]
;;                     [:th "Subscribe?"]]]
                   [:tbody
                    (for [a (db/show-signatories!)]
                      (html 
                       [:tr
                        [:td  (:name a)]
                        
;                        [:td  (:email a)]
                        [:td  (:org a)]]))]]]]]))
;;                        [:td  (:subscribe a)]]))]]]]]))
                       

(parser/set-resource-path!  (clojure.java.io/resource "html"))
(parser/add-tag! :csrf-field (fn [_ _] (anti-forgery-field)))
(filters/add-filter! :markdown (fn [content] [:safe (md-to-html-string content)]))

(defn render
"renders the HTML template located relative to resources/html"
[template & [params]]
(content-type
(ok
 (parser/render-file
  template
  (assoc params
         :page template
         :csrf-token *anti-forgery-token*)))
"text/html; charset=utf-8"))

(defn error-page
"error-details should be a map containing the following keys:
   :status - error status
   :title - error title (optional)
   :message - detailed error message (optional)

   returns a response map with the error page as the body
   and the status specified by the status key"
[error-details]
{:status  (:status error-details)
:headers {"Content-Type" "text/html; charset=utf-8"}
:body    (parser/render-file "error.html" error-details)})
