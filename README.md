# Ludus

Ludus (loo-das) is a participatory web platform for communities of user groups, enthusiasts, hobbyists, movements, advocacy etc. It aims to be an experiment in radical transparency and collaborative action.

## LICENSE

AGPLv3 with exceptions for Eclipse Public License

## Architecture Design

### Tech Stack

Clojure based web template called Luminus Web

